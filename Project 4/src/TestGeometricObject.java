/*
 * 
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * The Class TestGeometricObject.
 * <p>
 * This demonstrates the Project 4 classes by comparing them and stuff.
 */
public class TestGeometricObject {

	/**
	 * The main method.
	 *
	 * @param args moot/formality
	 */
	public static void main(String[] args) {
		ArrayList<GeometricObject> geoObjects = new ArrayList<>();
		geoObjects.add(new Circle(15, "white", true));
		geoObjects.add(new Rectangle(15, 10, "brown", false));
		geoObjects.add(new Trapezium(10, 50, 5.1, 25, "blue", true));
		geoObjects.add(new Circle(10));
		geoObjects.add(new Rectangle(20, 1));
		geoObjects.add(new Trapezium(20, 50, 25, 35));
		System.out.println("geoObjects:");
		printList(geoObjects);
		Collections.sort(geoObjects);
		System.out.println("Sorted:");
		printList(geoObjects);

	}

	/**
	 * Display geometric object.
	 *
	 * @param o the GeometricObject
	 */
	public static void displayGeometricObject(GeometricObject o) {
		StringBuilder output = new StringBuilder();
		output.append("The area is " + o.getArea());
		output.append("\nThe perimeter is " + o.getPerimeter());
		System.out.println(output.toString());
	}
	
	/**
	 * Prints the list.
	 *
	 * @param list the list
	 */
	public static void printList(List<GeometricObject> list) {
		System.out.println(list.size() + " objects:");
		for (GeometricObject each : list) {
			System.out.println(list.indexOf(each) + ": " + each);
			displayGeometricObject(each);
			System.out.println();
		}
	}

}
