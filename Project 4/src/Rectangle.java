
/**
 * The Class Rectangle.
 * <p>
 * Subclass of GeometricObject for rectangles. Adds height and width to usable
 * fields.
 */
	@SuppressWarnings("hiding")
public class Rectangle extends GeometricObject {

	/** The width. */
	private double width;
	
	/** The height. */
	private double height;
	
	/**
	 * Instantiates a new rectangle.
	 */
	public Rectangle() {
		
	}

	/**
	 * Instantiates a new rectangle with more data.
	 *
	 * @param width the width
	 * @param height the height
	 * @param color the color
	 * @param filled the filledness
	 */
	public Rectangle(double width, double height, String color, boolean filled) {
		super(color, filled);
		setWidth(width);
		setHeight(height);
	}

	/* (non-Javadoc)
	 * @see GeometricObject#getArea()
	 */
	@Override
	public double getArea() {
		return getHeight()*getWidth();
	}

	/* (non-Javadoc)
	 * @see GeometricObject#getPerimeter()
	 */
	@Override
	public double getPerimeter() {
		return (getHeight()+getWidth())*2;
	}
	
	/**
	 * Instantiates a new rectangle.
	 *
	 * @param width the width
	 * @param height the height
	 */
	public Rectangle(double width, double height) {
		setWidth(width);
		setHeight(height);
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public double getWidth() {
		return this.width
				;
	}

	/**
	 * Sets the width.
	 *
	 * @param width the new width
	 */
	public void setWidth(double width) {
		this.width = width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public double getHeight() {
		return this.height;
	}

	/**
	 * Sets the height.
	 *
	 * @param height the new height
	 */
	public void setHeight(double height) {
		this.height = height;
	}
}
