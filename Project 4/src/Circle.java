/**
 * The Class Circle.
 * <p>
 * Subclass of GeometricObject for circles. Adds radius to usable fields.
 */
@SuppressWarnings("hiding")
public class Circle extends GeometricObject {

	/** The radius. */
	private double radius;

	/**
	 * Instantiates a new circle.
	 */
	public Circle() {

	}

	/**
	 * Instantiates a new circle.
	 *
	 * @param radius the radius
	 * @param color the color
	 * @param filled the filled
	 */
	public Circle(double radius, String color, boolean filled) {
		super(color, filled);
		setRadius(radius);
	}

	/* (non-Javadoc)
	 * @see GeometricObject#getArea()
	 */
	@Override
	public double getArea() {
		return Math.PI * getRadius() * getRadius();
	}

	/**
	 * Gets the radius.
	 *
	 * @return the radius
	 */
	public double getRadius() {
		return this.radius;
	}

	/**
	 * Sets the radius.
	 *
	 * @param radius the new radius
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}

	/* (non-Javadoc)
	 * @see GeometricObject#getPerimeter()
	 */
	@Override
	public double getPerimeter() {
		return Math.PI * getDiameter();
	}

	/**
	 * Gets the diameter.
	 *
	 * @return the diameter
	 */
	public double getDiameter() {
		return getRadius() * 2;
	}

	/**
	 * Prints the circle.
	 */
	public void printCircle() {
		System.out.println("The circle is created " + getDateCreated()
				+ " and the radius is " + getRadius());
	}

	/**
	 * Instantiates a new circle.
	 *
	 * @param radius the radius
	 */
	public Circle(double radius) {
		super();
		setRadius(radius);
	}
}
