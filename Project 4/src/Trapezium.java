/**
* The Class Trapezium.
* <p>
* Subclass of GeometricObject for trapeziums. Adds length, width, sideA, sideB
* to usable fields.
*/
@SuppressWarnings("hiding")
public class Trapezium extends GeometricObject {
	
	/** The height. */
	private double height;
	
	/** The width. */
	private double width;
	
	/** The side a. */
	private double sideA;
	
	/** The side b. */
	private double sideB;
	
	/**
	 * Instantiates a new trapezium.
	 */
	public Trapezium() {
		
	}

	/**
	 * Instantiates a new trapezium with more info.
	 *
	 * @param height the height
	 * @param width the width
	 * @param sideA the side a
	 * @param sideB the side b
	 */
	public Trapezium(double height, double width, double sideA, double sideB) {
		setHeight(height);
		setWidth(width);
		setSideA(sideA);
		setSideB(sideB);
	}
	
	/**
	 * Instantiates a new trapezium with even more info.
	 *
	 * @param height the height
	 * @param width the width
	 * @param sideA the side a
	 * @param sideB the side b
	 * @param color the color
	 * @param filled the filled
	 */
	public Trapezium(double height, double width, double sideA, double sideB, String color, boolean filled) {
		super(color, filled);
		setHeight(height);
		setWidth(width);
		setSideA(sideA);
		setSideB(sideB);
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public double getHeight() {
		return this.height;
	}

	/**
	 * Sets the height.
	 *
	 * @param height the new height
	 */
	public void setHeight(double height) {
		this.height = height;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public double getWidth() {
		return this.width;
	}

	/**
	 * Sets the width.
	 *
	 * @param width the new width
	 */
	public void setWidth(double width) {
		this.width = width;
	}

	/**
	 * Gets the side a.
	 *
	 * @return the side a
	 */
	public double getSideA() {
		return this.sideA;
	}

	/**
	 * Sets the side a.
	 *
	 * @param sideA the new side a
	 */
	public void setSideA(double sideA) {
		this.sideA = sideA;
	}

	/**
	 * Gets the side b.
	 *
	 * @return the side b
	 */
	public double getSideB() {
		return this.sideB;
	}

	/**
	 * Sets the side b.
	 *
	 * @param sideB the new side b
	 */
	public void setSideB(double sideB) {
		this.sideB = sideB;
	}
	

	/* (non-Javadoc)
	 * @see GeometricObject#getArea()
	 */
	@Override
	public double getArea() {
		return (getSideA()+getSideB())*getHeight()/2;
	}

	/* (non-Javadoc)
	 * @see GeometricObject#getPerimeter()
	 */
	@Override
	public double getPerimeter() {
		return getSideA()+getSideB()+2*Math.sqrt(Math.pow(getHeight(), 2)+Math.pow((getSideB()-getSideA()),2)/4);
	}
}
