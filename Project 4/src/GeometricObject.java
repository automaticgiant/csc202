import java.util.Date;


/**
 * GeometricObject superclass
 * <p>
 * Project 4 is a Comparable and inheritance demo. This is the GeometricObject
 * class with the addition of an implemented compareTo, displayGeometricObject.
 * It is the superclass of Circle, Rectangle, and Trapezium.
 * 
 * @author Hunter Morgan
 */
@SuppressWarnings("hiding")
public abstract class GeometricObject implements Comparable<GeometricObject> {
	
	/**
	 * string to store object's color
	 */
	private String color = "white";

	/**
	 * boolean to store object's state of fill
	 */
	private boolean filled;
	
	/**
	 * java.util.Date to store object's date of creation
	 */
	private Date dateCreated;
	
	@Override
	public int compareTo(GeometricObject o) {
		return (int) (this.getArea()-o.getArea());
	}
	
	/**
	 * default constructor. provides no information other than creation date.
	 */
	protected GeometricObject() {
		this.dateCreated = new Date();
	}
	
	/**constructor which sets color and filled status in addition to
	 * timestamping
	 * @param color color for new object
	 * @param filled filled status for new object
	 */
	protected GeometricObject(String color, boolean filled) {
		this.dateCreated = new Date();
		this.color = color;
		this.filled = filled;
	}
	
	/**accessor for color
	 * @return color string
	 */
	public String getColor() {
		return this.color;
	}
	
	/**mutator for color
	 * @param color color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	
	/**boolean style accessor for filled status
	 * @return filled truth
	 */
	public boolean isFilled() {
		return this.filled;
	}
	
	/**mutator for filled boolean
	 * @param filled to fill or not to fill, that is the question
	 */
	public void setFilled(boolean filled) {
		this.filled = filled;
	}
	
	/**timestamp accessor
	 * @return timestamp java.util.Date
	 */
	public Date getDateCreated() {
		return this.dateCreated;
		}
	
	@Override
	public String toString() {
		return this.getClass().getName() /*+ " " + "created on " + this.dateCreated*/ + ", " + this.color + " and filled: " + this.filled;
	}
	
	/**accessor for type, rather the class name, which does describe the shape
	 * @return shape string(essentially, at least as long as the sub-classes are
	 * named sensibly)
	 */
	public String getType(){
		return this.getClass().getName();
	}
	
	/**abstract accessor for area - shape-dependent
	 * @return double containing computed area
	 */
	public abstract double getArea();
	/**abstract accessor for perimeter - shape-dependent
	 * @return double containing computed perimeter
	 */
	public abstract double getPerimeter();	
}
