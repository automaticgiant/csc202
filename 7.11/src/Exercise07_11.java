import java.util.Scanner;


/**
 * @author Hunter Morgan
 * week 3 - 7.11 solution
 */
public class Exercise07_11 {

	/**
	 * main routine to do the deed
	 * @param args moot / formality
	 */
	public static void main(String[] args) {
		boolean[][] matrix = new boolean[3][3];
		@SuppressWarnings("resource")	/* have eclipse set to warn if resource
										not managed with a try with resource
										construct. want to start adding these
										on later projects.
										*/
		Scanner stdin = new Scanner(System.in);
		System.out.print("Enter a number between 0 and 511:");
		String input = stdin.nextLine(); // take number in - no validation
		// what is this below? hmm...
		// I turn a string into an integer into a binary string into a char[]
		char[] bin = Integer.toBinaryString(Integer.parseInt(input)).toCharArray();
		int place = 0;
		// then fill in the matrix from LSB up, until i run out of binary digits
		for ( int row = 2 ; row > -1 ; row-- ) {
			for ( int column = 2 ; column > -1 ; column-- ) {
				place++; //keep track of which digit we are on
				if ( bin.length - place > -1 ) {
					// here we turn the 1's into booleans
					matrix[row][column] = (bin[bin.length-place] == '1');
				}
			}
		}
		
		// simple 2d array print nested for construct
		for ( boolean[] row : matrix ) {
			for ( boolean element : row ) {
				if (element) System.out.print("T ");
				else System.out.print("H ");
			}
			System.out.println(); // start next row on its own line
		}
		stdin.close(); // for tidiness
	}
}