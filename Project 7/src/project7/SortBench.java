package project7;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;

import org.dyndns.automaticgiant.useful.math.Random;

/**
 * The Class SortBench.<p>
 * 
 * This tests execution time of 4 sort algorithms, 5x each, and displays data.
 */
public class SortBench {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws Exception the exception
	 */
	public static void main(String[] args) throws Exception {
		//you can toggle this off to disable most of the status output, but you
		//might not know what it's doing while it's running and the run time is
		//kind of long
		boolean verbose = true;
		//it runs each data set through each algo 5 times and averages them later
		int trials = 5;
		//defines the data set sizes we are testing
		int[] sizes = { 25000,
				//switching the end of the array to this next line cuts down on
				//run-time for debugging
				//100000 };
				50000, 75000, 100000, 125000, 150000 };
		//defines the methods we are testing (actually wrappers that standardize
		//the interface)
		Method[] methods = {
				SortBench.class.getDeclaredMethod("selectionSortWrapper",
						int[].class),
				SortBench.class.getDeclaredMethod("bubbleSortWrapper",
						int[].class),
				SortBench.class.getDeclaredMethod("mergeSortWrapper",
						int[].class),
				SortBench.class.getDeclaredMethod("quickSortWrapper",
						int[].class) };
		//this is the data structure prototype for storing results
		HashMap<Method, HashMap<Integer, Long[]>> results = new HashMap<>();
		//we fill it out here
		for (Method each : methods) {
			results.put(each, new HashMap<Integer, Long[]>());
			for (int size : sizes)
				results.get(each).put(size, new Long[trials]);
		}

		//the outer loop is run based on set size
		for (int size : sizes) {
			//i have a bunch of these test output statements, that are kind of nice
			if (verbose)
				System.out.println("--Size: " + size);
			//iterates through trials number of trials
			for (int i = 0; i < trials; i++) {
				if (verbose)
					System.out.println("-Trial: " + (i + 1) + "/" + trials);
				//we use the same data for a given trial so that the comparison is better
				int[] unsorted = Random.randomBucket(1, 1000, size);
				for (Method method : methods) {
					if (verbose)
						System.out.print("Method: " + method.getName() + ", ");
					//since the algos work on arrays in-place, need a working copy
					int[] toSort = Arrays.copyOf(unsorted, size);
					long preTime = System.currentTimeMillis();
					method.invoke(null, toSort);
					long postTime = System.currentTimeMillis();
					results.get(method).get(size)[i] = postTime - preTime;
					if (verbose)
						System.out.println("Time: " + (postTime - preTime)
								+ "ms");
				}
			}
		}

		System.out
				.println("Array size\tSelection Sort\tBubble Sort\tMerge Sort\tQuick Sort");
		for (int i = 0; i < 80; i++)
			System.out.print("-");
		System.out.println();
		for (int size : sizes) {
			System.out.print(size + "\t\t");
			for (Method method : methods) {
				long sum = 0;
				for (long time : results.get(method).get(size)) {
					sum += time;
				}
				long average = sum / results.get(method).get(size).length;
				System.out.print(average + "ms\t\t");
			}
		System.out.println();
		}

	}

	/**
	 * Selection sort wrapper.
	 *
	 * @param array the array
	 */
	public static void selectionSortWrapper(int array[]) {
		selectionSort(array, array.length);
	}

	/**
	 * Bubble sort wrapper.
	 *
	 * @param array the array
	 */
	public static void bubbleSortWrapper(int array[]) {
		bubbleSort(array, array.length);
	}

	/**
	 * Merge sort wrapper.
	 *
	 * @param array the array
	 */
	public static void mergeSortWrapper(int array[]) {
		mergeSort(array, 0, array.length - 1);
	}

	/**
	 * Quick sort wrapper.
	 *
	 * @param array the array
	 */
	public static void quickSortWrapper(int array[]) {
		quickSort(array, 0, array.length - 1);
	}

	/**
	 * Selection sort.
	 *
	 * @param array the array
	 * @param n the number of items
	 */
	public static void selectionSort(int array[], int n) {
		//iterate through the whole list
		for (int x = 0; x < n; x++) {
			//looking for the minimum
			int index_of_min = x;
			//we have a marker, y, for the end of our ordered sublist
			for (int y = x; y < n; y++) {
				if (array[index_of_min] < array[y]) {
					index_of_min = y;
				}
			}
			//then we swap the next higher number into it
			int temp = array[x];
			array[x] = array[index_of_min];
			array[index_of_min] = temp;
		}
	}

	/**
	 * Bubble sort.
	 *
	 * @param a the a
	 * @param n the number of items
	 */
	public static void bubbleSort(int a[], int n) {
		int i, j, t = 0;
		//step through the list
		for (i = 0; i < n; i++) {
			//if we've gone all the way through, then we have completely bubbled
			//1 item - can do fewer next time 
			for (j = 1; j < (n - i); j++) {
				//if they are out of order, swap them
				if (a[j - 1] > a[j]) {
					t = a[j - 1];
					a[j - 1] = a[j];
					a[j] = t;
				}
			}
		}
	}

	/**
	 * Merge sort.<p>
	 * 
	 * This is recursive, so arguments matter.
	 *
	 * @param array the array
	 * @param lo the low end of the sort range
	 * @param n the high end of the sort range
	 */
	public static void mergeSort(int array[], int lo, int n) {
		int low = lo;
		int high = n;
		//if we can't subdivide, bail
		if (low >= high) {
			return;
		}

		//we subdivide each range
		int middle = (low + high) / 2;
		//recurse to subdivisions
		mergeSort(array, low, middle);
		mergeSort(array, middle + 1, high);
		//last index of first section
		int end_low = middle;
		//first index of second section
		int start_high = middle + 1;
		//while we are still subdivided
		while ((lo <= end_low) && (start_high <= high)) {
			//recombine the one way
			if (array[low] < array[start_high]) {
				low++;
			//or recombine the other way
			} else {
				//reverse through the first section
				int Temp = array[start_high];
				for (int k = start_high - 1; k >= low; k--) {
					array[k + 1] = array[k];
				}
				array[low] = Temp;
				//keep going
				low++;
				end_low++;
				start_high++;
			}
		}
	}

	/**
	 * Quick sort.
	 *
	 * @param numbers the numbers
	 * @param low the low end
	 * @param high the high end
	 */
	private static void quickSort(int[] numbers, int low, int high) {
		int i = low, j = high;

		//pick a pivot
		int pivot = numbers[low + (high - low) / 2];

		while (i <= j) {

			while (numbers[i] < pivot) {
				i++;
			}

			while (numbers[j] > pivot) {
				j--;
			}

			//swap to align the numbers around the pivot
			if (i <= j) {
				int temp = numbers[i];
				numbers[i] = numbers[j];
				numbers[j] = temp;
				i++;
				j--;
			}
		}

		//recurse on each half
		if (low < j)
			quickSort(numbers, low, j);
		if (i < high)
			quickSort(numbers, i, high);
	}

}
