import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;


/**This'n records and outputs grades from a temporary file for educational
 * purposes.
 * @author Hunter Morgan
 *
 */
 public class Project3 {

	
	/**
	 * method to encapsulate and write out an ArrayList to a RandomAccessFile
	 * @param records ArrayList to write
	 * @param fileHandle RandomAccessFile to write to
	 * @throws Exception don't want to catch
	 */
	public static void writeFile(ArrayList<Student> records, RandomAccessFile fileHandle) throws Exception {
		ByteArrayOutputStream byteArrayOutHandle = new ByteArrayOutputStream();
		ObjectOutputStream objectOutStream = new ObjectOutputStream(byteArrayOutHandle);
		objectOutStream.writeObject(records);
		for (byte each : byteArrayOutHandle.toByteArray()) fileHandle.write(each);
	}

	/**
	 * method to read a RandomAccessFile and de-encapsulate the ArrayList
	 * @param fileHandle RandomAccessFile to read from
	 * @return student ArrayList
	 * @throws Exception don't want to catch
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList<Student> readFile(RandomAccessFile fileHandle) throws Exception {
		byte[] inputBytes = new byte[(int) fileHandle.length()];
		for (int i=0; i<fileHandle.length(); i++) 
			inputBytes[i] = fileHandle.readByte();
		ByteArrayInputStream byteArrayInHandle = new ByteArrayInputStream(inputBytes);
		ObjectInputStream objectInHandle = new ObjectInputStream(byteArrayInHandle);
		return (ArrayList<Student>) objectInHandle.readObject();
	}
	
	
	/**
	 * this is the main method. it does the object stuff and calls the read and write methods
	 * @param args formality/moot
	 * @throws Exception don't want to catch
	 */
	@SuppressWarnings("boxing")
	public static void main(String args[]) throws Exception{
		File file = new File("poop.bin");
		@SuppressWarnings("resource")
		RandomAccessFile fileHandle = new RandomAccessFile(file, "rw");
		@SuppressWarnings("resource")
		Scanner stdin = new Scanner(System.in);
		int numberOfStudents;
		System.out.print("Please enter number of students: ");
		numberOfStudents = Integer.valueOf(stdin.nextLine());
		ArrayList<Student> records = new ArrayList<>();
		for (int i=0;i!=numberOfStudents;i++){
			records.add(new Student());
			System.out.print("Please enter the first name for student" + (i+1) + ": ");
			records.get(i).setFirstName(stdin.nextLine());
			System.out.print("Please enter the last name for student" + (i+1) + ": ");
			records.get(i).setLastName(stdin.nextLine());
			for (int n=0;n!=3;n++){
				System.out.format("Please enter score%d for student%d: ", n+1, i+1);
				records.get(i).addGrade(Double.valueOf(stdin.nextLine()));
			}
		}
		stdin.close();
		writeFile(records, fileHandle);
		fileHandle.seek(0);
		records=null;
		records=readFile(fileHandle);
		fileHandle.close();
		DecimalFormat decimalFormat = new DecimalFormat("#.##");
		int highest = 0;
		double highestGrade = 0;
		for (int n = 0; n<numberOfStudents;n++) 
			if (records.get(n).getAverage()>highestGrade) {
				highest = n;
				highestGrade = records.get(n).getAverage();
			}
		for (int n = 0; n < numberOfStudents; n++){
			System.out.format("Student%d: %s\nScores: ", n+1, records.get(n).getFullName());
			for (int i = 0;i<3;i++)
				System.out.print(decimalFormat.format(Double.valueOf(records.get(n).getGrades().get(i))) + " ");
			System.out.print("\nAverage: " + decimalFormat.format(Double.valueOf(records.get(n).getAverage())) + "%   ");
			if (n == highest) System.out.print("(Highest Average)");
			System.out.println();
		}
	}
 }


/*decided my Project 3 compromise approach was avoidable. I
first started with a student class and was going to have OO goodness,
but thought it infeasible due to RandomAccessFile requirement. Did a
stupid way, then realized I could still use RAF to write objects if I
serialized them separately. I'm all over it now. I want to finish my
compromise crappy code and submit, but I know I can pull of a rewrite in
classtime.
*/
//public class Project3 {
//	static Scanner stdin = new Scanner(System.in);
//	static int numberOfStudents;
//
//	/**The method to the madness.
//	 * @param args moot/formality
//	 * @throws IOException don't care to catch
//	 */
//	@SuppressWarnings("resource")
//	public static void main(String[] args) throws IOException {
//		File temporaryFileName = new File(String.valueOf(Integer.valueOf((int) (Math.random()*10000))));
//		RandomAccessFile temporaryFile = new RandomAccessFile(temporaryFileName, "rw");
//		writeFile(temporaryFile);
//		readFile(temporaryFile);
//		temporaryFile.close();
//	}
//
//	/**
//	 * @param temporaryFile file handle
//	 * @throws IOException don't care to catch
//	 */
//	@SuppressWarnings("boxing")
//	public static void writeFile(RandomAccessFile temporaryFile) throws IOException {
//		System.out.print("Please enter number of students: ");
//		numberOfStudents = Integer.valueOf(stdin.nextLine());
//		double[] averages = new double[numberOfStudents];
//		for (int i=0;i!=numberOfStudents;i++){
//			System.out.print("Please enter the first name for student" + (i+1) + ": ");
//			String name = stdin.nextLine();
//			System.out.print("Please enter the last name for student" + (i+1) + ": ");
//			name.concat(" " + stdin.nextLine());
//			temporaryFile.writeUTF(name);
//			double sum = 0;
//			for (int n=0;n!=3;n++){
//				System.out.format("Please enter score%d for student%d: ", n+1, i+1);
//				double grade = stdin.nextDouble();
//				sum += grade;
//				temporaryFile.writeDouble(grade);
//			}
//			averages[i] = sum/3;
//		}
//		int highest = 0;
//		for (int n = 1; n<numberOfStudents;n++) {
//			if (averages[n]>averages[highest]) highest = n;
//		}
//		temporaryFile.write(highest);
//	}
//	
//	/**
//	 * @param temporaryFile file handle
//	 * @throws IOException don't care to catch
//	 */
//	@SuppressWarnings("boxing")
//	public static void readFile(RandomAccessFile temporaryFile) throws IOException {
//		temporaryFile.seek(temporaryFile.length()-1);
//		int highest=temporaryFile.read();
//		temporaryFile.seek(0);
//		for (int i=0;i!=numberOfStudents;i++){
//			System.out.format("Student%d: \nScores: ",i);
//			double average;
//			double sum = 0;
//			for (int n=0;i!=3;n++){
//				double grade = temporaryFile.readDouble();
//				sum =+  grade;
//				System.out.print(grade + "  ");
//			}
//			System.out.format("Average: %f.2% ", )
//		}
//	}
//}