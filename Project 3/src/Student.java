import java.io.Serializable;
import java.util.ArrayList;

	 /**
	  * This class stores all the data for each student and will calculate an
	  * average on command.
	  * @author Hunter Morgan
	  *
	  */
	@SuppressWarnings("serial")
	public class Student implements Serializable {
		/**
		 * ArrayList of Doubles representing grades
		 */
		private ArrayList<Double> grades;
		/**
		 * String for first name
		 */
		private String firstName;
		/**
		 * method to set firstName
		 * @param firstName first name
		 */
		@SuppressWarnings("hiding")
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		/**
		 * method to set last name
		 * @param lastName last name
		 */
		@SuppressWarnings("hiding")
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		/**
		 * String for last name
		 */
		private String lastName;
		/**
		 * method to retrieve for full name
		 * @return string with first and last name
		 */
		public String getFullName() {
			 return this.firstName + " " + this.lastName;
		 }
		/**
		 * method to compute average of grades
		 * @return double grades
		 */
		public double getAverage() {
			double sum = 0;
			for (double each : this.grades) sum+=each;
			return sum/this.grades.size();
		}
		/**
		 * method to add grade to list
		 * @param newGrade grade to add
		 * @return truth for add success
		 */
		public boolean addGrade(Double newGrade) {
			return this.grades.add(newGrade);
		}
		
		/**
		 * accessor for grades
		 * @return ArrayList<Double> grades
		 */
		public ArrayList<Double> getGrades() {
			return this.grades;
		}
		
		/**
		 * constructor
		 */
		public Student(){
			this.grades = new ArrayList<>();
		}
	 }
