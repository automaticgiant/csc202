import java.util.Scanner;
/**
 * Week 2 - Exercise 6.17 
 * @author Hunter Morgan
 */
public class Exercise06_17 {

	/**
	 * main routine to do the deed
	 * @param args moot/formality
	 */
	public static void main(String[] args) {
		//Scanner object for getting keyboard input 
		@SuppressWarnings("resource")	/* have eclipse set to warn if resource
										not managed with a try with resource
										construct. want to start adding these
										on later projects.
										*/
		Scanner input = new Scanner(System.in);
		System.out.println("Number of grades/names?");
		//set size
		int number = input.nextInt();
		//array for names
		String[] names = new String[number];
		//array for grades
		float[] grades = new float[number];
		//iteration variable
		int i;
		//fill the arrays
		for ( i = 0 ; i < number; i++ ) {
			System.out.println("Student name?");
			names[i] = input.next();
			System.out.println("Grade?");
			grades[i] = input.nextFloat();
		}
		//start the sort at the second pair
		int position = 1;
		//do this until we're at the end
		while (position < number) {
			//compare to the previous grade - <= governs sort order
			//if we are correctly sorted in decending order so far, continue
			if (grades[position] <= grades[position-1]) {
				position++;}
			else {
				//or push it back and try again - this is effectively swap(a,b)
				String tempName = names[position];
				float tempGrade = grades[position];
				names[position] = names[position-1];
				grades[position] = grades[position-1];
				names[position-1] = tempName;
				grades[position-1] = tempGrade;
				//unless we've pushed our number to the front, keep moving back 
				if (position > 1) position--;
				}
			}
		//now, we're sorted and we can provide an ordered list
		System.out.println("Name, Score");
		for ( i = 0 ; i < number ; i++ ) System.out.println(names[i]+", "+grades[i]);
		//for tidiness:
		input.close();
		}
}
