@SuppressWarnings("all")
public class Test2DArrays {

	public static void main(String[] args) throws InterruptedException {
		while (true) {
			print2DArray(create2DArray());
			System.out.println();
			Thread.sleep(500);
		}
	}

	public static int[][] create2DArray() {
		// make a new array!
		int[][] matrix = new int[(int) (Math.random()*30)+1][(int) (Math.random()*26)+1];
		// fill
		for (int row = 0; row < matrix.length; row++) {
			for (int column = 0; column < matrix[0].length; column++) {
				matrix[row][column] = (int) (Math.random() * 100);
			}
		}
		return matrix;
	}

	public static void print2DArray(int[][] array) {
		// print the array!
		System.out.println("Rows: " + array.length + " Columns: " + array[0].length);
		for (int row = 0; row < array.length; row++) {
			for (int column = 0; column < array[0].length; column++) {
				System.out.format("%02d ",array[row][column]);
			}
			// each row gets a newline
			System.out.println();
		}
	}
}