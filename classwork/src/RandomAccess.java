import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;


public class RandomAccess {

	@SuppressWarnings("boxing")
	public static void main(String[] args) throws IOException {
		Scanner stdin = new Scanner(System.in);
//		System.out.print("filename: ");
//		String filename = stdin.nextLine();
		String filename = "testdata.bin";
		RandomAccessFile fileHandle = new RandomAccessFile(filename, "rw");
		//System.out.printLine(fileHandle.getFD());

			System.out.print("Read existing? ");
			if (stdin.nextLine().toLowerCase().startsWith("y")) {
// this snippet, below, will print file as hex bytes
//				byte[] file = new byte[(int) fileHandle.length()];
//				fileHandle.read(file);
//				for (byte currentByte : file) {
//					System.out.printf("%02X ",currentByte);
				while ( fileHandle.getFilePointer() != fileHandle.length() ) { 
					System.out.print(fileHandle.readUTF() + ", a ");
					int sides = fileHandle.read();
					System.out.print(sides + "-gon, has an area of ");
					double sideLength = fileHandle.readDouble();
					double apothem = sides / ( 2 * Math.tan(Math.PI/sides));
					double area = sides * sideLength * apothem / 2;
					System.out.println(area + ".");
				}
			System.out.print("Keep? ");
			if (stdin.nextLine().toLowerCase().startsWith("y")) System.exit(0);
			fileHandle.setLength(0);

		{
				System.out.print("Number of regular polygons? ");
				int number = Integer.parseInt(stdin.nextLine());
				for ( int i = 0; i < number ; i++ ) {
					System.out.print("Name it: ");
					fileHandle.writeUTF(stdin.nextLine());
					System.out.print("Sides? ");
					fileHandle.write(Integer.parseInt(stdin.nextLine()));
					System.out.print("Side length? ");
					fileHandle.writeDouble(Double.parseDouble(stdin.nextLine()));
				}
				fileHandle.seek(0);
				while ( fileHandle.getFilePointer() != fileHandle.length() ) { 
					System.out.print(fileHandle.readUTF() + ", a ");
					int sides = fileHandle.read();
					System.out.print(sides + "-gon, has an area of ");
					double sideLength = fileHandle.readDouble();
					double apothem = sides / ( 2 * Math.tan(Math.PI/sides));
					double area = sides * sideLength * apothem / 2;
					System.out.println(area + ".");
				}

			}
		}
	}
}