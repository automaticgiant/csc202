import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.Scanner;

@SuppressWarnings("javadoc")
class NGon {
	public String name;
	public int sides;
	public double sideLength;
}


@SuppressWarnings("javadoc")
public class SerializingRandomAccess {
static String filename;
	static void read() throws IOException {
		ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(filename));
		NGon currentObject = objectInputStream.read();
		while (  ) { 
			System.out.print(objectInputStream.readUTF() + ", a ");
			int sides = objectInputStream.read();
			System.out.print(sides + "-gon, has an area of ");
			double sideLength = objectInputStream.readDouble();
			double apothem = sides / ( 2 * Math.tan(Math.PI/sides));
			double area = sides * sideLength * apothem / 2;
			System.out.println(area + ".");
		}

	}
	
	public static void main(String[] args) throws IOException {
		Scanner stdin = new Scanner(System.in);
//		System.out.print("filename: ");
//		String filename = stdin.nextLine();
		String filename = "testdata.bin";
		objectInputStream = new ObjectInputStream(filename, "rw");
		//System.out.printLine(fileHandle.getFD());

			System.out.print("Read existing? ");
			if (stdin.nextLine().toLowerCase().startsWith("y")) {
// this snippet, below, will print file as hex bytes
//				byte[] file = new byte[(int) fileHandle.length()];
//				fileHandle.read(file);
//				for (byte currentByte : file) {
//					System.out.printf("%02X ",currentByte);
				read();
			System.out.print("Keep? ");
			if (stdin.nextLine().toLowerCase().startsWith("y")) System.exit(0);
			objectInputStream.setLength(0);

	// prompt\write
		{
			System.out.print("Number of regular polygons? ");
			int number = Integer.parseInt(stdin.nextLine());
			NGon[] objects = new NGon[number];
			Arrays.fill(objects, new NGon());
			for ( int i = 0; i < number ; i++ ) {
				System.out.print("Name it: ");
				objects[i].name = stdin.nextLine();
				System.out.print("Sides? ");
				objects[i].sides = Integer.parseInt(stdin.nextLine());
				System.out.print("Side length? ");
				objects[i].sideLength = Double.parseDouble(stdin.nextLine());
			}
			read();
			}
		}
	}
}