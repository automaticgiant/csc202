package Chapter22;

import java.util.TreeSet;
import java.util.Set;

public class SetOperations {

	public static void main(String[] args) {

		//generate two sets of 10 random integers
		TreeSet<Integer> set2 = new TreeSet<>();
		for (int i=1; set2.size()!=10 ; i++)
			set2.add(Integer.valueOf((int) (Math.random()*50)));
		TreeSet<Integer> set1 = new TreeSet<>();
		for (int i=1; set1.size()!=10 ; i++)
			set1.add(Integer.valueOf((int) (Math.random()*50)));

		//placeholder for set operations
		TreeSet<Integer> tempSet;

		//show both initial sets
		System.out.println("set2:");
		printSet(set2);
		System.out.println("set1:");
		printSet(set1);
		System.out.println("set2 -difference- set1");
		tempSet=(TreeSet<Integer>) set1.clone();
		tempSet.removeAll(set2);
		printSet(tempSet);
		System.out.println("set2 -Union- set1");
		tempSet=(TreeSet<Integer>) set1.clone();
		tempSet.addAll(set2);
		printSet(tempSet);
		System.out.println("set2 -Intersection- set1");
		tempSet=(TreeSet<Integer>) set1.clone();
		tempSet.retainAll(set2);
		printSet(tempSet);
		
		
	}
	public static void printSet(Set<Integer> set) {
		System.out.println(set.size() + " items, " + set);
		System.out.println();
	}

}
