package Chapter26;

import java.util.NoSuchElementException;

public class MyLinkedList<E> {
	Node<E> head;
	Node<E> tail;
	int size;

	private static class Node<E> {
		Node<E> next;
		Node<E> previous;
		E content;
		public String toString() {
			return content.toString();
		}
		public Node(E e) {
			this.content = e;
		}
	}
	
	public void addFirst(E e) {
		Node<E> insert=new Node<E>(e);
		Node<E> temp = head;
		head = insert;
		if (tail == null) tail = insert;
		if (temp != null) {
			insert.next = temp;
			temp.previous=insert;
		}
		size++;
	}
	
	public void addLast(E e) {
		Node<E> insert=new Node<E>(e);
		Node<E> temp = tail;
		tail = insert;
		if (head == null) head = insert;
		if (temp != null) {
			temp.next = insert;
			insert.previous=temp;
		}
		size++;
	}
	
	public void add(E e, int index) {
		if (size == 0 || index == 0) addFirst(e);
		else if (!(index < size)) addLast(e);
		else {
		Node<E> insert=new Node<E>(e);
		Node<E> temp = get(index);
		insert.previous=temp.previous;
		insert.next=temp;
		temp.previous.next=insert;
		size++;
		}
	}
	
	public Node<E> get(int index) {
		if (index < 0 || index > size -1 ) throw new NoSuchElementException();
		Node<E> current = head;
		for (int i = 0; i < index; i++)	current = current.next;
		return current;
	}
	
	public String toString() {
		String out ="[";
		Node<E> current = head;
		while (current != null) {
			out+=current.content;
			if (current.next != null) out += ", ";
			current=current.next;
		}
		out+="]";
		return out;
	}
}
	

