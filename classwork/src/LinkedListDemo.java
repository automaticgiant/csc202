import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;


public class LinkedListDemo {

	public static void main(String[] args) {

		
		List<Double> list1 = new LinkedList<>();
		//list1.addAll({10.5, 5.1});
		//list1.add();
		list1.add(10.5);
		list1.add(16.3);
		list1.add(82.5);
		list1.add(45.1);
		list1.add(29.9);
		list1.add(92.6);
		list1.add(77.7);
		list1.add(50.2);
		list1.add(10.5);
		list1.add(71.9);
		list1.add(60.5);
		
		System.out.println("List1:");
		displayList(list1);
		list1.add(3,100.0);
		System.out.println("List1 with 100.0 inserted @ 3:");
		displayList(list1);
		System.out.println("List1 backwards:");
		displayBackwardsList(list1);
		list1.remove(0);
		System.out.println("List1 without item 0:");
		displayList(list1);
		System.out.println("List1 with last item changed to 30.0:");
		list1.set(list1.size()-1, 30.0);
		displayList(list1);
		System.out.println("List1 without anything >50:");
		removeBigs(list1);
		displayList(list1);
		System.out.println("List1, sorted");
		Collections.sort(list1);
		displayList(list1);
		

	}
	
	public static void removeBigs(List plist){
		Iterator<Double> it = plist.iterator();
		while (it.hasNext()) {
			if (it.next() > 50.0) it.remove();
		}
	}
public static void displayList(List<Double> plist){
	Iterator<Double> it = plist.iterator();
	while (it.hasNext()){
		System.out.print(it.next() + ", ");
	}
	System.out.println();
}
public static void displayBackwardsList(List<Double> plist){
	ListIterator<Double> it = plist.listIterator(plist.size());
	while (it.hasPrevious()){
		System.out.print(it.previous() + ", ");
	}
	System.out.println();
}

}
