import java.util.Stack;


public class StackDemo {
	public static void main (String[] args) {
		Stack<String> stack = new Stack<>();
		stack.push("Selvi");
		stack.push("Chaky");
		stack.push("Ramya");
		stack.push("Chaitra");
		System.out.println(stack.capacity());
		System.out.println(stack.peek() + stack.peek() + stack.pop() + stack.peek());
		while (!stack.empty()) System.out.println(stack.pop());
	}
}
