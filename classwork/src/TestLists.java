import java.util.*;

public class TestLists {
	public static void main(String[] args) {
		Integer[] array1 = { 34, 29, 50, 67, 90, 100, 62, 67 };
		//ArrayList<Integer> list1 = (ArrayList<Integer>) Arrays.asList(array1);
		List<Integer> list1 = Arrays.asList(array1);
		
		System.out.println("list1: " + list1);
		System.out.println("list1.get(3): " + list1.get(3));
		System.out.println("list1.indexOf(67): " + list1.indexOf(67));
		System.out.println("list1lastIndexOf(67): " + list1.lastIndexOf(67));
		System.out.println("list1.set(1, 1)");
		list1.set(1, 1);
		System.out.println("list1: " + list1);
		System.out.println("list1 is array-backed: array1[1] = list1.get(1)");
		System.out.println("array1: " + Arrays.asList(array1));
		
		Collections.sort(list1);
		System.out.println(list1);
		System.out.println("size of list1 is " + list1.size());
		
		List<Double> list2 = new ArrayList<>();
		list2.add(5.6);
		list2.add(13.5);
		list2.add(45.2);
		list2.add(9.9);
		System.out.println(list2);
		Collections.sort(list2);
		System.out.println(list2);
		System.out.println("size of list2 is " + list2.size());
	}
}
