import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;


public class MapDemo {
	public static void main(String[] args) {
		Map<Integer, String> studentMap = new LinkedHashMap<>();
		studentMap.put(56789, "john");
		studentMap.put(98973, "james");
		studentMap.put(23498, "Joe");
		studentMap.put(39784, "phil");
		studentMap.put(12563, "han");
		System.out.println(studentMap);
		studentMap = new TreeMap(studentMap);
		System.out.println(studentMap);  
		studentMap = new HashMap(studentMap);
		System.out.println(studentMap);
		}
}
