import java.util.Vector;


public class VectorDemo {
	public static void main(String[] arg) {
		Vector<String> vector = new Vector<>();
		vector.add("Selvi"); //true if it works
		vector.addElement("Chaky"); //void
		vector.addElement("Selvi");
		vector.addElement("Bhanu");
		
		System.out.println(vector);
		vector.removeElement("Selvi");
		System.out.println(vector);
		vector.removeElement("Selvi");
		System.out.println(vector);
		vector.removeElementAt(1);
		System.out.println(vector);
		vector.insertElementAt("Selvi", 0);
		System.out.println(vector);
		vector.addAll(vector);
		System.out.println(vector);
		System.out.println(vector.size());
		System.out.println(vector.capacity());
		
		
		
	}
}
