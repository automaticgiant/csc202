import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;


public class QueueDemo {

	public static void main(String[] args) {
		Queue<Integer> queue = new PriorityQueue<>();
		queue.offer(10);
		queue.offer(5);
		queue.offer(25);
		queue.offer(16);
		queue.offer(12);
		queue.remove(5);
		
		System.out.println("queue:");
		emptyQueue(queue);
		queue.offer(234);
		queue.clear();
		if (queue.peek() == null) System.out.println("queue is empty");
		
		
	}
	public static void emptyQueue(Queue<Integer> plist){
		while (plist.peek() != null){
			System.out.print(plist.remove() + ", ");
		}
		System.out.println();
	}


}
