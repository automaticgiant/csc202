import java.util.Scanner;


@SuppressWarnings("all")
public class NextAndNextLine {

	public static void main(String[] args) {
		Scanner stdin = new Scanner(System.in);
		System.out.print("string1(next): ");
		String string1 = stdin.next();
		stdin.nextLine(); // the newline needs eaten
		System.out.print("string2(nextLine): ");
		String string2 = stdin.nextLine();
		System.out.println();
		System.out.print(string1 + " " + string2);
		stdin.close();
	}

}
