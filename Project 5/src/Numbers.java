import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * The Class Numbers generates a list of random numbers in the given range and
 * talks about them.
 */
public class Numbers {

	/**
	 * The main method contains all the logic, save a reusable number generator
	 * method.
	 *
	 * @param args moot/formality/unimplemented
	 */
	@SuppressWarnings("boxing") //what do you think about auto-boxing? good, bad?
	public static void main(String[] args) {
		//Frequency map, sorted
		Map<Integer, Integer> frequencyMap = new TreeMap<>();
		
		@SuppressWarnings("resource") // something has to be very wrong to not have stdin
		Scanner stdin = new Scanner(System.in);
		
		System.out.print("How many random numbers do you want to generate?");
		int quantity = Integer.valueOf(stdin.nextLine());
		
		System.out.print("What is the range of random numbers?");
		String input = stdin.nextLine();
		stdin.close();
		//I use a RegEx and Matcher on the input string to tolerate varied input
		Matcher intMatcher = Pattern.compile("\\d+").matcher(input);
		intMatcher.find();
		int floor = Integer.valueOf(intMatcher.group());
		intMatcher.find();
		int ceiling = Integer.valueOf(intMatcher.group());
		
		//Call the random method to get integers
		Integer[] bucket = (Integer[]) randomBucket(floor, ceiling, quantity);
		
		//Populate the frequency map.
		for (Integer each : bucket) {
			if (!frequencyMap.containsKey(each))
				frequencyMap.put(each, 0);
			frequencyMap.put(each, frequencyMap.get(each)+1);
		}
		
		//Display the integers
		System.out.println("The " + quantity + " random numbers in the range of " + floor + " - " + ceiling + " are:\n");
		for (int i = 0; i < bucket.length; i++) {
			System.out.print(bucket[i]);
			if (i+1 < bucket.length) System.out.print(", ");
			//Add a newline if you've output 20
			if ((i+1)%20==0) System.out.println();
		}
		
		//Display frequencies
		System.out.println();
		for (Entry<Integer, Integer> entry : frequencyMap.entrySet()) {
			System.out.println(entry.getKey() + " occurs \t" + entry.getValue() + " times.");
		}

		//Display sorted Integers
		System.out.println("\nAfter sorting the numbers are:\n");
		for (Entry<Integer, Integer> entry : frequencyMap.entrySet()) {
			for (int i = 0; i < entry.getValue(); i++)
				System.out.print(entry.getKey() + ", ");
			System.out.println();
		}
	}

	/**
	 * Random bucket generator.<p>
	 * This is designed to be able to be implemented polymorphically. This
	 * implementation generates an array of integers within the specified
	 * constraints.
	 *
	 * @param floor the floor
	 * @param ceiling the ceiling
	 * @param quantity the quantity
	 * @return the Number[], array of Integers
	 */
	@SuppressWarnings("boxing")
	public static Number[] randomBucket(Integer floor, Integer ceiling, Integer quantity) {
		Integer[] bucket = new Integer[quantity];
		Random prng = new Random();
		for (int i = 0; i<quantity; i++) {
				bucket[i] = prng.nextInt(ceiling-floor+1)+floor;
			}
		return bucket;
		
	}
	
}
