import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringBufferInputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;


/**
 * The Class PrimeCompare.<BR>
 * 
 * This class measures execution time of unmodified book code. I decided it
 * would be more beneficial to spend the time I would have used to port the book
 * code to easily callable secondary methods or figuring out a clean way to
 * instantiate the book code classes instead learning about something about
 * which I was already curious, reflection.
 */
@SuppressWarnings("deprecation")
public class PrimeCompare {
	
	/**
	 * The main method.<BR>
	 * 
	 * It hard-codes the classes we will time, gets Method objects for their
	 * main methods, and uses a timing method to benchmark them for 10 different
	 * prime ceilings.
	 *
	 * @param args moot/formality
	 * @throws Exception one of many possible exceptions that I won't catch
	 */
	@SuppressWarnings({ "javadoc", "resource", "boxing" })
	public static void main(String[] args) throws Exception {
		//temporary list to hold main methods
		List<Method> methodList = new ArrayList<>();
		methodList.add(Class.forName("chapter23.PrimeNumbers").getDeclaredMethod("main", String[].class));
		methodList.add(Class.forName("chapter23.EfficientPrimeNumbers").getDeclaredMethod("main", String[].class));
		methodList.add(Class.forName("chapter23.SieveOfEratosthenes").getDeclaredMethod("main", String[].class));
		//actual data structure we will use to store results - nested maps, to organize by class/test subject
		Map<Method, Map<Integer, Long>> methodResults = new LinkedHashMap<>();
		//populate result data structure with proper nested maps
		for ( Method each : methodList ) methodResults.put(each,new HashMap<Integer, Long>());
		//done with this, mark for cleanup
		methodList = null;
		//header
		System.out.println("\t\t\tPrimeNumbers\t\tEfficientPrimeNumbers\tSieveOfErastosthenes");
		//outer loop controls n iteration
		for ( int n = 1; n < 11; n++) {
			System.out.print("Less than " + n*100000 + "\t");
			//inner loop tests specified methods with particular n
			for ( Method each : methodResults.keySet() ) {
				methodResults.get(each).put(n,
						//this call with the anonymous StringBufferInputStream
						//sets up the input for the main method's Scanner
						timeMethodExecution( each, new StringBufferInputStream(n*100000 + "\n")));
			//then we fetch the result from the data structure and print
				System.out.print(methodResults.get(each).get(n) + "\t\t\t");
			}
			System.out.println();
		}
	}

		/**
		 * Time method execution.<BR>
		 * 
		 * This method does some stdio redirection to allow for unmodified code
		 * to be run. It uses the specified inputstream as input and overrides
		 * the write method on the OutputStream class to effectively pipe output
		 * to /dev/null. After timing, it reverts the streams to default.
		 *
		 * @param timedMethod the method we want to time
		 * @param inputstream the inputstream we want to feed it
		 * @return the long time in milliseconds
		 * @throws Exception the exception
		 */
		public static long timeMethodExecution(Method timedMethod, InputStream inputstream) throws Exception {
		//placeholders for standard in/out streams
		InputStream stdin = System.in;
		PrintStream stdout = System.out;
		//effects redirecting output of test runs to null
		System.setOut(new PrintStream( new OutputStream() {public void write( int b ){}}));
		System.setIn(inputstream);
		long startTime = System.currentTimeMillis();
		timedMethod.invoke(null, new String[1]);
		System.setOut(stdout);
		System.setIn(stdin);
		long endTime = System.currentTimeMillis(); 
		return endTime - startTime;
	}

}
