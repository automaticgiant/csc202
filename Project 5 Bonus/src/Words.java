import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Scanner;
import java.util.TreeMap;



/**
 * The Class Words.<p>
 * This is the bonus variation of Project 5. It takes a single line of text or a
 * file passed as the sole argument and counts word frequency for the document.
 * Parsing is imperfect, but good enough. Interesting fact: I didn't realize the
 * assignment directed modification of the textbook example until after I had
 * implemented it myself. Consequently, it parses a little differently, though I
 * hope that since the data structure was the real purpose, that will not count
 * against me.<p>
 * The data structure was puzzled about for quite a bit until an epiphany struck
 * during the bus ride home. The conventional (at least it is what I did last
 * semester and what the book does) implementation of a frequency map as a
 * hash table, or in this case TreeMap, is supplemented by an additional TreeMap
 * with HashSet values to store frequencies and words with those frequencies.
 * TreeMap's inherent natural ordering facilitates description of the data by
 * alphabetical and frequency orders, respectively.<p>
 * The two data structures are processed, during the data-in phase, in parallel,
 * for not rigorously tested, but apparently good performance. Simple enhanced
 * for loops are used to display the collected data.
 */
public class Words {

	/**
	 * The main method. All the magic happens here. You can see a dead end where
	 * I tried to use Scanner to most of the parsing work, but, as usual, could
	 * not get Java's RegEx implementation to work. (Silly Java, RegEx is for
	 * Perl.) More explanation is contained in in-line comments.
	 *
	 * @param args Only one argument is supported: the name of an optional input file.
	 * @throws FileNotFoundException An exception is thrown when Scanner can't
	 * find the file (and can't find the backup file, War and Peace) because
	 * adding a War and Peace failsafe was easy and I didn't want to go further.
	 */
	@SuppressWarnings({ "boxing", "resource" })
	public static void main(String[] args) throws FileNotFoundException {
		Scanner parser;
		// if there exists 1 argument, it is the file name
		if (args.length == 1)
			try {
				parser = new Scanner(new File(args[0]));
			} catch (FileNotFoundException e) {
				parser = new Scanner(Words.class.getResourceAsStream("pg2600.txt"));
			}
		else {
			Scanner stdin = new Scanner(System.in);
			System.out.println("Please enter a few sentences. The words and their number of occurrences will be reported:");
			// yes, I fed a Scanner with a Scanner. It is what it is.
			parser = new Scanner(stdin.nextLine());
			stdin.close();
		}
		// this maps words to frequencies and keeps organized by alphabet
		Map<String, Integer> wordCounts = new TreeMap<>();
		// this maps the reverse - frequencies to words - and is numerically ordered
		Map<Integer, Set<String>> countWords = new TreeMap<>();
		// this stuff didn't work
//		Pattern wordPattern = Pattern.compile("\\p{Lower}+|(\\p{Lower}+\\S\\p{Lower}+)");
//		Matcher wordMatcher = wordPattern.matcher("");

		// this is the main input parsing/processing loop
		while (parser.hasNext()) {
//			wordMatcher.reset(parser.next().toLowerCase());
//			String token = wordMatcher.group();
			// first operation is to lowercase it
			String token = parser.next().toLowerCase();


			// flag for alpha testing - this loop makes sure that the token contains 1 or more letters or numbers
			boolean hasLetters = false;
			for (char each : token.toCharArray()) {
				if (Character.isLetterOrDigit(each)) {
					hasLetters = true;
					break;
				}
			}
			if (!hasLetters) continue;
			
			// these two loops trim punctuation from the head and tail of tokens
			while (!Character.isAlphabetic(token.toCharArray()[token.length()-1]) && (token.length() > 1))
				token=token.substring(0, token.length()-1);
			while (!Character.isAlphabetic(token.toCharArray()[0]) && (token.length() > 1))
				token=token.substring(1, token.length());
			
			// this logic maintains the TreeMaps, adding keys, maintaining frequency sets, etc.
			if (!wordCounts.containsKey(token))
				wordCounts.put(token, 0);
			int count = wordCounts.get(token);
			wordCounts.put(token, count+1);
			if (countWords.containsKey(count)) 
				countWords.get(count).remove(token);
			if (!countWords.containsKey(count+1))
				countWords.put(count+1, new HashSet<String>());	
			countWords.get(count+1).add(token);
		}
		parser.close();
		
		
		// output loops
		System.out.println("Here are the words in alphabetical order along with their number of occurrences.");
		for (String each : wordCounts.keySet())
			System.out.println("The word \"" + each + "\" occurs " + wordCounts.get(each) + " time.");
		
		System.out.println("Here are the words in ascending order of their number of occurrences.");
		for (Integer eachCount : countWords.keySet())
			for (String eachWord : countWords.get(eachCount))
				System.out.println(eachCount + " time the word " + eachWord + " occurs.");

	
	}
}
